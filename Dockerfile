FROM alpine:3.10

MAINTAINER wangkun23 <845885222@qq.com>

ENV VERSION=7.8.1

WORKDIR /usr/share/metricbeat
RUN apk add --update-cache curl bash libc6-compat && \
    rm -rf /var/cache/apk/* && \
    curl https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-${VERSION}-linux-x86_64.tar.gz -o metricbeat.tar.gz && \
    tar xzvf metricbeat.tar.gz && \
    rm metricbeat.tar.gz && \
    mv metricbeat-${VERSION}-linux-x86_64/* /usr/share/metricbeat/

VOLUME /usr/share/metricbeat/data

CMD ["./metricbeat","-e"]
